# -*- coding: utf-8 -*-
"""
Replicate topiramate butte paper
http://stm.sciencemag.org/content/3/96/96ra76.full
"""
#%%
import numpy as np
import dill
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm
import os

BASE = "/home/gstupp/projects/repositioning"
os.chdir(BASE)
import DDS

#%% Parse GEO SOFT
df_GEO = DDS.parse_SOFT(os.path.join(BASE,"GDS1615/GDS1615.soft"))
df_GEO.to_csv(os.path.join(BASE,"GDS1615/df.csv"))

#%% get sample data relationship
sample_type = DDS.parse_SDRF(os.path.join(BASE,"GDS1615/E-GEOD-3365.sdrf.txt"))
df_crohn_normal = df_GEO[pd.Series(sorted(list(sample_type["Crohn's disease"] | sample_type["normal"])))]
DDS.write_SAM_csv(df_crohn_normal, sample_type["normal"], sample_type["Crohn's disease"], os.path.join(BASE,"GDS1615/df_crohn_normal.csv"))

#%% Calculte FC of GEO data, to compare to butte table 1
entrez_gene = dill.load(open(os.path.join(BASE,'entrez_gene','rb')))
diff_df = pd.DataFrame(np.log2(df_GEO[pd.Series(list(sample_type["Crohn's disease"]))].astype(float)).mean(1) - \
                        np.log2(df_GEO[pd.Series(list(sample_type["normal"]))].astype(float)).mean(1))
diff_df = diff_df.groupby(level=0,).mean()

diff_df['symbol'] = diff_df.index
diff_df['entrez'] = [entrez_gene.get(x,None) for x in diff_df.index]
diff_df.index = diff_df.entrez
diff_df=diff_df.rename(columns = {0:'calc_fc'})
diff_df = diff_df[~pd.isnull(diff_df.index)]
#%% Compare with butte table s1
butte_entrez = pd.read_excel(os.path.join(BASE,"butte_top_paper/Table_S1.xls"))
butte_entrez.columns = ['entrez','Topiramate CMAP Rank','Crohns mRNA Expression Level']
butte_entrez.index = butte_entrez['entrez']
del butte_entrez['entrez']

df_butte_GEO = diff_df.join(butte_entrez)
df_butte_GEO['log_butte_crohns'] = np.log2(df_butte_GEO['Crohns mRNA Expression Level'])
df_butte_GEO = df_butte_GEO[~pd.isnull(df_butte_GEO.index)]
df_butte_GEO=df_butte_GEO.dropna()
plt.scatter(df_butte_GEO.calc_fc,df_butte_GEO.log_butte_crohns)
plt.xlabel("Chron's affected vs. Chron's unaffected LogFC")
plt.ylabel("Crohn's mRNA Expression Level (Butte, Log)")
# label the top 4
for _,row in df_butte_GEO.sort("calc_fc").iloc[:2].iterrows():
    plt.text(row.calc_fc,row.log_butte_crohns,row.symbol,bbox=dict(facecolor='red', alpha=0.5))
for _,row in df_butte_GEO.sort("calc_fc").iloc[-2:].iterrows():
    plt.text(row.calc_fc,row.log_butte_crohns,row.symbol,bbox=dict(facecolor='red', alpha=0.5))
for _,row in df_butte_GEO.sort("log_butte_crohns").iloc[:2].iterrows():
    plt.text(row.calc_fc,row.log_butte_crohns,row.symbol,bbox=dict(facecolor='red', alpha=0.5))
for _,row in df_butte_GEO.sort("log_butte_crohns").iloc[-2:].iterrows():
    plt.text(row.calc_fc,row.log_butte_crohns,row.symbol,bbox=dict(facecolor='red', alpha=0.5))
#%%  Connectivity map
pgm = pd.DataFrame.from_csv(os.path.join(BASE,"pgm.csv")) # probe-gene mapping
probe_gene = {row['probe_id']: row['symbol'] for gene,row in pgm.iterrows()}
drugs_df = pd.DataFrame.from_csv(os.path.join(BASE,"connmap/rankMatrix.txt", sep='\t'))
del drugs_df['Unnamed: 6101']
drugs_df = drugs_df[pd.notnull(drugs_df.index)]
drugs_df.index = [probe_gene.get(x,None) for x in drugs_df.index]

# get connectivity map ID -> drug name
cmap_meta = pd.read_csv(os.path.join(BASE,"connmap/cmap_instances_02.csv"))
cmap_id_name = {x["instance_id"]:x["cmap_name"] for _,x in cmap_meta.iterrows()}

#%%
drug = '915'
genes_drug_rank = drugs_df[drug].groupby(level=0).mean().sort_values(inplace=False)
genes_drug_rank = {gene:n for n,gene in enumerate(genes_drug_rank.index)}

#%% get Crohn's up/down genes
# Done with SAM
up_df = pd.read_csv("/home/gstupp/projects/repositioning/GDS1615/SAM/up.csv")
up_df = up_df[up_df['q-value(%)']==0].sort('Fold Change',ascending=False)
up = list(up_df[up_df['Fold Change']>=2]['Gene ID'])

down_df = pd.read_csv("/home/gstupp/projects/repositioning/GDS1615/SAM/down.csv")
down_df = down_df[down_df['q-value(%)']==0].sort('Fold Change',ascending=True)
down = list(down_df[down_df['Fold Change']<=(1/2)]['Gene ID'])

DDS.calc_dds(genes_drug_rank, up, down)


#%% Or pick the top N
up = set(diff_df.sort("calc_fc").symbol.iloc[:34])
down = set(diff_df.sort("calc_fc").symbol.iloc[-20:])
DDS.calc_dds(genes_drug_rank, up, down)
#%% All drugs
dds = {}
for drug in tqdm(drugs_df.columns[:454]):
    genes_drug_rank = drugs_df[drug].groupby(level=0).mean().sort_values(inplace=False)
    genes_drug_rank = {gene:n for n,gene in enumerate(genes_drug_rank.index)}
    dds[drug] = DDS.calc_dds(genes_drug_rank, up, down)
dds2 = sorted([(x[0],abs(x[1])) for x in dds.items()], key=lambda x:x[1], reverse=True)
dds2 = sorted([(cmap_id_name[x[0]],x[1]) for x in dds.items()], key=lambda x:x[1], reverse=True)
#%%

