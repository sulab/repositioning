
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def calc_dds(genes_drug_rank, up, down):
    """
    Connectivity Analytics 
    Method for generating a drug-disease score (dds)
    http://science.sciencemag.org/content/sci/suppl/2006/09/29/313.5795.1929.DC1/Lamb.SOM.pdf
    http://stm.sciencemag.org/content/scitransmed/3/96/96ra77.full.pdf
    http://stm.sciencemag.org/content/3/96/96ra76.full
    
    genes_drug_rank: Dict[gene name: rank-normalized rank]
    up, down: list of gene names, sorted by fc
    """
    
    up_gene_rank = sorted([genes_drug_rank[gene] for gene in up if gene in genes_drug_rank])
    down_gene_rank = sorted([genes_drug_rank[gene] for gene in down if gene in genes_drug_rank])
    
    a_up = max([(j/len(up_gene_rank)) - (V_j/len(genes_drug_rank)) for j,V_j in enumerate(up_gene_rank)])
    a_down = max([(j/len(down_gene_rank)) - (V_j/len(genes_drug_rank)) for j,V_j in enumerate(down_gene_rank)])
    
    b_up = max([(V_j/len(genes_drug_rank)) - ((j-1)/len(up_gene_rank)) for j,V_j in enumerate(up_gene_rank)])
    b_down = max([(V_j/len(genes_drug_rank)) - ((j-1)/len(down_gene_rank)) for j,V_j in enumerate(down_gene_rank)])
    
    es_up = a_up if a_up > b_up else -1*b_up
    es_down = a_down if a_down > b_down else -1*b_down
    
    dds = 0 if (es_up>0 and es_down>0) or (es_up<0 and es_down<0) else es_up - es_down
    return dds


def parse_SOFT(soft_path):    
    f = open(soft_path)
    for line in f:
        if line.strip() == "!dataset_table_begin":
            break
    
    headers = next(f).strip().split()
    data = [dict(zip(headers,line.strip().split('\t'))) for line in f if line != "!dataset_table_end"]
    df = pd.DataFrame(data)
    df.index = df.IDENTIFIER
    del df['IDENTIFIER']
    del df['ID_REF']
    df = df.astype(float)
    return df
    
def parse_SDRF(sdrf_path):
    sdrf=pd.read_csv(open(sdrf_path),sep='\t')
    samples = [x[x.index("GSM"):] for x in sdrf['Source Name']]
    sample_type = {k:{samples[x] for x in v} for k,v in sdrf.groupby('Characteristics [DiseaseState]').groups.items()}
    return sample_type
    
def write_SAM_csv(df_crohn_normal, group1, group2, out_path):
    rename = {k:1 for k in group1}
    rename.update({k:2 for k in group2})
    df_crohn_normal_csv = df_crohn_normal.rename(columns = rename)
    df_crohn_normal_csv.to_csv(out_path)