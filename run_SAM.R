
data = read.csv("GDS1615/df_crohn_control3.csv")
genenames = data$X
genenames = lapply(genenames, as.character)
geneid = data$X
data = data[, !(names(data) %in% c("X","X.1"))]
y=c(rep(1,59), rep(2,41))
samfit = SAM(data, y, nperms = 100, genenames = genenames, resp.type="Two class unpaired")
write.csv(samfit$siggenes.table$genes.up,file = "GDS1615/SAM/up.csv")
write.csv(samfit$siggenes.table$genes.lo,file = "GDS1615/SAM/down.csv")

