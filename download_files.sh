
# connectivity map
mkdir -p connmap
cd connmap
wget ftp://ftp.broad.mit.edu/pub/cmap/rankMatrix.txt.zip
wget http://www.broadinstitute.org/cmap/cmap_instances_02.xls

# GEO
mkdir -p GDS1615
cd GDS1615
wget ftp://ftp.ncbi.nlm.nih.gov/geo/datasets/GDS1nnn/GDS1615/soft/GDS1615.soft.gz
gunzip GDS1615.soft.gz


